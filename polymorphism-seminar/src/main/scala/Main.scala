import java.security.SecureRandom

def square(x: Int) = x * x

def square(x: Double) = x * x

def const[T](x: T): Any => T = { _ => x }

def applyFunction[A, B](x: A)(f: A => B): B = f(x)

class Item
class ComputerDevice extends Item
class Keyboard(val deviceName: String = "default keyboard")
    extends ComputerDevice

def constItem[A >: ComputerDevice <: Item](currentPage: A): A = currentPage

@main def test =
  println(square(2))
  println(square(2d))
  val function: Int => String = i => i.toString + " decimal"
  val const1 = const[Double](1)
  println(applyFunction(2)(function))
//  println(constItem(new Item))
//  println(constItem(new ComputerDevice))
//  println(constItem(new Keyboard: ComputerDevice))
  val item1: Item = new Item
  val computerDevice1: ComputerDevice = new ComputerDevice
  val computerDevice2: Item = new ComputerDevice
  val keyboard1: Item = new Keyboard
  val item2: Item =
    if (SecureRandom.getInstanceStrong.nextBoolean()) new Item else new Keyboard
  println(constItem(item2))
