val scala3Version = "3.2.0"

val zioGroupName = "dev.zio"
val zioDependencies = Seq(
  ("zio", "2.0.2"),
  ("zio-streams", "2.0.2")
).map { case (p, version) => zioGroupName %% p % version }

lazy val `polymorphism-seminar` = project
  .in(file("polymorphism-seminar"))
  .settings(scalaVersion := scala3Version)

lazy val root = project
  .in(file("."))
  .settings(
    name := "polymorphism",
    version := "0.1.0-SNAPSHOT",
    scalaVersion := scala3Version,
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % "3.2.14" % Test
    ) ++ zioDependencies,
    Compile / mainClass := Some("ZioMain")
  )
  .dependsOn(`polymorphism-seminar`)
  .enablePlugins(JavaAppPackaging)

ThisBuild / resolvers += "mvn proxy" at "https://nexus.tcsbank.ru/repository/mvn-maven-proxy"
scalacOptions ++= Seq("-Werror")
