import zio.ZIO

import java.util.Random

object FunctionalProgramming {

  var i: Int = 0

  def save(item: Item): Unit = {
    MyList.runningLocally = {
      true || false && true
    }

    if (i >= 10) {
      return
    }

    if (MyList.runningLocally) {
      saveToFile(item)
      i += 1
    } else {
      saveToDatabase(item)
    }
  }

  def doSomethingUseful(a: Int, b: Int): Either[ServiceError, Double] = {
    Right(())
    div(a, b)
      .flatMap(v =>
        if (a + b == 0) Left(ArithmeticError("Division by zero"))
        else Right(v.toDouble / (a + b))
      )
      .flatMap(v =>
        if (a + b == 0) Left(ArithmeticError("Division by zero"))
        else Right(v / (a + b))
      )
  }

//  def doSomethingUseful2(a: Int, b: Int): ZIO[DatabaseService | GenRandom, ServiceError, Double] = {
//    Right(())
//    div(a, b)
//      .flatMap(v =>
//        if (a + b == 0) Left(ArithmeticError("Division by zero"))
//        else Right(v.toDouble / (a + b))
//      )
//      .flatMap(v =>
//        if (a + b == 0) Left(ArithmeticError("Division by zero"))
//        else Right(v / (a + b))
//      )
//  }

  @main def eitherFlatMap =
    println(doSomethingUseful(3, 0))
    println(doSomethingUseful(3, -3))
    println(doSomethingUseful(-7, 5))

  @main def showFlatMap =
    println(Seq(1, 2, 3).flatMap(i => Seq.fill(i)(i)))
    println(Seq(1, 2, 3).flatMap(i => Seq(i, i)))
    println(Seq(1, 2, 3).flatMap(i => Seq("number " + i.toString, "and then")))
    println(Seq(1, 2, 3).map(i => Seq("number " + i.toString)))

  def div(a: Int, b: Int): Either[ServiceError, Int] =
    if (b == 0) Left(ArithmeticError("Division by zero")) else Right(a / b)

  def saveToFile(item: Item) = ???
  def saveToDatabase(item: Item) = ???
}

sealed trait ServiceError

case class ArithmeticError(errorMessage: String) extends ServiceError

trait Monad[F[_]] {
  def pure[A](a: A): F[A]
  def flatMap[A, B](fa: F[A])(f: A => F[B]): F[B]
}

given Monad[Option] with {
  override def pure[A](a: A): Option[A] = Some(a)

  override def flatMap[A, B](fa: Option[A])(f: A => Option[B]): Option[B] =
    fa match
      case Some(value) => f(value)
      case None        => None
}

// forall f . pure(a).flatMap(f) == f(a)
// forall o: Option[_] . o.flatMap(i => pure(i)) == o
// o.flatMap(f).flatMap(g) == o.flatMap(i => f(i).flatMap(g))
