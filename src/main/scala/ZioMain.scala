import ZioMain.myApp
import zio.*

import java.io.IOException

object ZioMain extends ZIOAppDefault {

  def printHello: ZIO[String, IOException, Unit] = {
    val name: ZIO[String, Nothing, String] = ZIO.service[String]
    name
      .flatMap(fullName =>
        Console.printLine("Hello, " + fullName + ", good to meet you!")
      )
  }

  // ZIO[-R, +E, +A] === R => Either[E, A]
  // ZIO.service[R]: (x: R) => Right(x)
  val myApp: ZIO[Any, IOException, Unit] =
    for {
      _ <- Console.printLine("Hello! What is your name?")
      firstName <- Console.readLine
      _ <- Console.printLine("And what is your last name?")
      lastName <- Console.readLine
      fullName = firstName + " " + lastName
      _ <- printHello.provide(ZLayer(ZIO.succeed(fullName)))
    } yield ()

//    Console
//      .printLine("Hello! What is your name?")
//      .flatMap(_ =>
//        Console.readLine
//          .flatMap(firstName =>
//            Console
//              .printLine("And what is your last name?")
//              .flatMap(lastName =>
//                Console.printLine(
//                  "Hello, " + firstName + " " + lastName + ", good to meet you!"
//                )
//              )
//          )
//      )

  override def run: ZIO[Any, IOException, Unit] = myApp
}
