sealed trait MyList[+A] {
  def size: Int
}

object MyList {
  var runningLocally = {
    true || false && true
  }
}

case class Cons[A](head: A, tail: MyList[A]) extends MyList[A] {
  override def size: Int = 1 + tail.size
}
case class MyNil[A]() extends MyList[A] {
  override def size: Int = 0
}

trait Validator[-A] {
  def isValid(a: A): Boolean
}

class ItemValidator extends Validator[Item] {
  override def isValid(a: Item): Boolean = true
}

class KeyboardValidator extends Validator[Keyboard] {
  override def isValid(a: Keyboard): Boolean =
    !a.deviceName.equals("default keyboard")
}

trait MyFunction[-A, +B] {
  def apply(arg: A): B
}

trait Codec[A] {
  def encode(a: A): String
  def decode(s: String): Either[String, A]
}

@main def optionTest =
  var paymentAmountFromUser: Option[Double] = Some(3d)
  paymentAmountFromUser = paymentAmountFromUser.map(_ * 2)

  // amount is present => fee = 10%
  // amount is absent => fee = 1
  def fee(paymentAmountFromUser: Option[Double]): Double =
    paymentAmountFromUser match {
      case Some(x: Double) => x * 0.1
      case None            => 1d
    }

  val items: MyList[Item] =
    Cons(new Item, Cons(new Keyboard, Cons(new ComputerDevice, MyNil())))
  val keyboards: MyList[Item] = Cons(new Keyboard, MyNil())
  println(items.size)
  println(keyboards.size)

  def sendToWarehouse(items: MyList[Item]): Unit = println(
    items.toString + " sent to warehouse"
  )
  def validateItemWithMessage(validator: Validator[Item])(
      item: Item
  ): Option[String] =
    if (validator.isValid(item)) None else Some(s"item $item is invalid")
  def validateKeyboardWithMessage(
      validator: Validator[Keyboard]
  )(keyboard: Keyboard): Option[String] =
    if (validator.isValid(keyboard)) None
    else Some(s"keyboard $keyboard is invalid")

  validateItemWithMessage(new ItemValidator())(new Item)
//  validateItemWithMessage(new KeyboardValidator())
  validateKeyboardWithMessage(new ItemValidator())(new Keyboard)
  validateKeyboardWithMessage(new KeyboardValidator())(new Keyboard)
  val v: Validator[Keyboard] = new ItemValidator()
  val l: List[Item] = List[Keyboard](new Keyboard, new Keyboard)
//  val v1: Validator[Item] = new KeyboardValidator()
//  val l1: List[Keyboard] = List(new Item())

  sendToWarehouse(items)
  sendToWarehouse(keyboards)

  val f1: Item => Item = ???
  val f2: Keyboard => Keyboard = ???

  // val f3: Item => Keyboard = f2
  // val f4: Item => Item = f2
  val f5: Keyboard => Item = f2
  val f6: Keyboard => Item = f1
//   val f7: Keyboard => Keyboard = f1
