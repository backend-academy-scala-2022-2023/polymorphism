trait Ord[-T]:
  def compare(x: T, y: T): Int
  extension (x: T)
    def <(y: T) = compare(x, y) < 0
    def >(y: T) = compare(x, y) > 0

def greater[T](a: T, b: T)(using u: Ord[T]) = u.compare(a, b) < 0

def max[T](xs: Seq[T])(using u: Ord[T]): Option[T] = {
  xs.foldLeft(Option.empty) {
    case (None, a)    => Some(a)
    case (Some(a), b) => if (a < b) Some(a) else Some(b)
  }
}

given Ord[Int] with
  override def compare(x: Int, y: Int): Int =
    if x < y then -1 else if x > y then +1 else 0

given listOrd[T](using ord: Ord[T]): Ord[List[T]] with
  def compare(xs: List[T], ys: List[T]): Int = (xs, ys) match
    case (Nil, Nil) => 0
    case (Nil, _)   => -1
    case (_, Nil)   => +1
    case (x :: xs1, y :: ys1) =>
      val fst = ord.compare(x, y)
      if fst != 0 then fst else compare(xs1, ys1)

extension (x: String) def <(y: String): Boolean = x.length < y.length

@main def contextParameters =
  println(List(3, 2, 2) > List(3, 2, 1))
  println(greater(List(3, 2, 2), List(3, 2, 1)))
